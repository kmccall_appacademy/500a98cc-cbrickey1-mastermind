
class Code

  attr_reader :pegs

  PEGS = {
    'R' => :red,
    'G' => :green,
    'B' => :blue,
    'Y' => :yellow,
    'O' => :orange,
    'P' => :purple
  }


  def initialize(arr)
    @pegs = arr
    #code =>  array of symbols like [:blue, :blue, :blue, :blue] b/c rspec tests are sending code through parser before initializing
    #code.pegs => array of corresponding letters ['B', 'B', 'B', 'B']
  end


  #makes a new instance of code that is an array of symbols
  def Code.parse (str)
    array_of_letters = str.upcase.split("")
    array_of_color_symbols = array_of_letters.map do |l|
      if PEGS[l] == nil
        raise 'Invalid peg'
      else
        PEGS[l]
      end
    end

    #make a new code with the array of symbols (calls it @pegs)
    Code.new(array_of_color_symbols)
  end


  def Code.random
    array = Array.new
    4.times { array << PEGS.keys.sample }
    Code.new(array)
  end


  def [](index)
    @pegs[index]
  end


  #guess comes in as an array of symbols b/c comes via .parse
  def exact_matches(guess)
    count = 0
    (0..3).each do |index|
      count += 1 if self[index].downcase == guess[index].downcase
    end
    count
  end


  #guess comes in as an array of symbols b/c comes via .parse
  def near_matches(guess)
    this_array = self.pegs
    that_array = guess.pegs.dup

    count = 0
    (0...this_array.length).each do |index|
      this_item = this_array[index]
      if that_array.include?(this_item)
        that_array = that_array - [this_item]
        count += 1
      end
    end

    count -= exact_matches(guess)
    count < 0 ? 0 : count
  end


  def ==(guess)
    return true if exact_matches(guess) == 4
    false
  end

end #of class



class Game

  attr_reader :secret_code


  def initialize(code=Code::random)
    @secret_code = code
  end


  def get_guess
    input = $stdin.gets.chomp

    while input.length != 4 || input.chars.any? { |l| Code::PEGS.include?(l.upcase) == false }
      puts "Bad format. Only use the letters R, G, B, Y, O, P.  No spaces or symbols."
      input = $stdin.gets.chomp
    end

    Code.new(input)
  end


  def display_matches(guess)

    if guess.exact_matches(@secret_code) == 4
      puts "You guessed correctly!"
      exit
    else
      puts """
        Your guess was incorrect.
        You guessed the exact color and position of #{guess.exact_matches(@secret_code)} pegs correctly.
        You guessed correct color, but wrong position (near match) for #{guess.near_matches(@secret_code)} pegs.
        Try again!
      """
    end

  end


  def play
    puts """
      The computer selected four colored pegs.
      Potential colors are red, green, blue, yellow, orange, and purple.
      You have ten chances to guees the correct colors and the order of the pegs.
      Guess the code, for example, 'RGBY' for red-green-blue-yellow.
    """

    10.times { display_matches(get_guess) }
    puts "You're out of guesses.  GAME OVER."
  end

end #of class


if __FILE__ == $0
  game = Game.new
  game.play
end
